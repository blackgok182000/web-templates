let a = 9;

console.log(a == 9); //! True

console.log(a === "9"); //! False

console.log(a != 9); //! False

console.log(a > 9); //! False

console.log(a >= 9); //!True

let b = 10;

console.log(a == 9 && b == 10); //! True

console.log(a == 9 && b != 10); //! False

